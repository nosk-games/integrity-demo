﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    [Serializable]
    class PlayerScoreList
    {
        public PlayerScore[] items;
    }
}
