﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class GameStats
    {
        private static string FILENAME = "game.sav";

        public static int coins = 0;
        public static int maxScore = 0;

        public static void save()
        {
            string path = Application.persistentDataPath + "/" + FILENAME;

            using (StreamWriter writer = new StreamWriter(path, false))
            {
                writer.WriteLine(coins.ToString());
                writer.WriteLine(maxScore.ToString());
                writer.Flush();
            }

        }

        public static void load()
        {
            string path = Application.persistentDataPath + "/" + FILENAME;
            Debug.Log(path);
            string strCoins = "";
            string strScore = "";
            try
            {
                using (StreamReader reader = new StreamReader(path, false))
                {
                    strCoins = reader.ReadLine();
                    strScore = reader.ReadLine();
                }

            }catch(Exception e)
            {
                Debug.Log("Save file not found");
            }

            if (!string.IsNullOrEmpty(strCoins))
            {
                coins = int.Parse(strCoins);
            }

            if (!string.IsNullOrEmpty(strScore))
            {
                maxScore = int.Parse(strScore);
            }

        }
    }

}
