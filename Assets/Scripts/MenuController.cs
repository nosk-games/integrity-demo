﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{

    public GameObject scoreboard;
    public Button moonButton;
    public Text moonText;
    public Text coins;

    // Start is called before the first frame update
    void Start()
    {
        GameStats.load();
        coins.text = GameStats.coins.ToString("000");

        if( DateTime.Now >= new DateTime(2021, 1, 1))
        {
            moonButton.interactable = true;
            moonText.text = "Play";
        }
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            exitGame();
        }
    }

    public void loadEarthScene()
    {
        SceneManager.LoadScene("earth", LoadSceneMode.Single);
    }

    public void loadMoonScene()
    {
        SceneManager.LoadScene("moon", LoadSceneMode.Single);
    }

    public void enableScoreboard()
    {
        scoreboard.SetActive(true);
    }

    public void disableScoreboard()
    {
        scoreboard.SetActive(false);
    }

    public void exitGame()
    {
        Application.Quit();
    }
}
