﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private const string JSON_TEMPLATE = "{{\"user\":\"{0}\",\"score\": {1} }}";
    public UnityEngine.UI.Text scoreText;
    public UnityEngine.UI.Text coinsText;
    public Transform[] coinSpawns;
    public Transform[] zombieSpawns;
    public GameObject gameOverUI;

    public UnityEngine.UI.Text playerName;
    public GameObject submitScoreObj;
    public GameObject submitedObj;


    [HideInInspector]
    public static GameManager instance;

    private float scoreRatio = 3.5f;
    private float score = 0;
    private int coins = 0;
    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        addScore();
    }


    public void addScore()
    {
        this.score += scoreRatio * Time.deltaTime ;
        scoreText.text = ((int)this.score).ToString();
    }

    public void addCoin()
    {
        this.coins++;
        coinsText.text = this.coins.ToString();
    }

    public void gameOver()
    {
        Time.timeScale = 0;
        gameOverUI.SetActive(true);
        GameStats.coins = GameStats.coins + this.coins;
        if (this.score > GameStats.maxScore)
        {
            GameStats.maxScore = (int)this.score;
        }
        GameStats.save();

    }

    public void backToMenu()
    {
        SceneManager.LoadScene("menu", LoadSceneMode.Single);
        Time.timeScale = 1;
    }

    public void submitScore()
    {
        string name = playerName.text;
        if (!string.IsNullOrWhiteSpace(name))
        {
            string data = string.Format(JSON_TEMPLATE, name, Math.Floor(score).ToString());
            byte[] bodyData = Encoding.UTF8.GetBytes(data);
            UnityWebRequest scoreboardRQ = new UnityWebRequest("http://scoreboard.dirtydevelopers.org:2015/score",
                UnityWebRequest.kHttpVerbPOST,
                new DownloadHandlerBuffer(), 
                new UploadHandlerRaw(bodyData));
            scoreboardRQ.SetRequestHeader("Content-Type", "application/json; charset=utf-8");
            scoreboardRQ.SendWebRequest();
            submitScoreObj.SetActive(false);
            submitedObj.SetActive(true);
        }
    }
}
